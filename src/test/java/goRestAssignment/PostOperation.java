package goRestAssignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class PostOperation {
	@Test
	public void testingPostOperation() {
		JSONObject req = new JSONObject();
		req.put("name", "Bharath");
		req.put("email", "eeebharathraj3@gmail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		ValidatableResponse Response = given().log().all().contentType("application/json")
				.header("authorization", "Bearer 956bbfbcab7df0694ce4e254606baefcab2afa8c02c7bdeb444f018ddd360ed2")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
		
		

	}
}
